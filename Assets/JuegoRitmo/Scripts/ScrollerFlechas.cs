﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollerFlechas : MonoBehaviour
{
    public float tempoFlechas;

    public bool hasStarted;

    // Start is called before the first frame update
    void Start()
    {
        tempoFlechas = tempoFlechas / 60f;
    }

    // Update is called once per frame
    void Update()
    {
     if (!hasStarted)
     {
         
     } else
     {
         transform.position -= new Vector3(0f,tempoFlechas*Time.deltaTime,0f);
     }  
    }
}
