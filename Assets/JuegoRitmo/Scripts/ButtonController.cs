﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour{

    private SpriteRenderer spriteRenderer;
    public Sprite botonNormal;
    public Sprite botonPulsado;

    public KeyCode keyPulsada;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(keyPulsada))
        {
            spriteRenderer.sprite = botonPulsado;
        }
        if (Input.GetKeyUp(keyPulsada))
        {
            spriteRenderer.sprite = botonNormal;
        }
    }
}
