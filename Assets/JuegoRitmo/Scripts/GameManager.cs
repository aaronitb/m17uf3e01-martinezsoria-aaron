﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public AudioSource musica;
    public AudioSource sonidoHit;

    public bool startMusica;

    public ScrollerFlechas scroller;

    public static GameManager instance;

    public int puntuacion;
    public int combo =1;

    public int puntuacionPorNota = 10;

    public Text textoPuntuacion;
    public Text textoMultiplicador;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        textoPuntuacion.text = "Puntuación: 0";
        textoMultiplicador.text = "Combo: x1";
    }

    // Update is called once per frame
    void Update()
    {
        if (!startMusica)
        {
            if (Input.anyKeyDown)
            {
                startMusica = true;
                scroller.hasStarted = true;
                musica.Play();
            }
        }
    }

    public void NoteHit(){
        puntuacion += puntuacionPorNota*combo;
        combo += 1;
        textoPuntuacion.text = "Puntuación: "+puntuacion;
        textoMultiplicador.text = "Combo: x"+combo;
        sonidoHit.Play();
    }

    public void NoteMissed(){
        Debug.Log("Nota fallada");
        combo = 1;
        textoMultiplicador.text = "Combo: x"+combo;
    }
}
