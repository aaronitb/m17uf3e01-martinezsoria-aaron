﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlechasController : MonoBehaviour
{

    public bool canBePressed;

    public KeyCode teclaPulsada;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(teclaPulsada))
        {
            if (canBePressed)
            {
                gameObject.SetActive(false);
                GameManager.instance.NoteHit();
            }
        }
        
    }

    private void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Activator")
        {
            canBePressed = true;
        }
        if (other.gameObject.name.Equals("DestruyeFlechas")){
            GameManager.instance.NoteMissed();
        }
    }

    private void OnTriggerExit2D(Collider2D destruir){
        if (destruir.tag == "Activator")
        {
            canBePressed = false;
        }
        
    }
}
